---
objet: Motif du courrier
lieu: No man's land
expéditeur:
  nom: Alice Doe
  email: alice@doe.org
  téléphone: numérodetelephone
  adresse:
    - numéro et rue
    - code postal ville
destinataire:
  nom: John Doe
  adresse:
    - numéro et rue
    - code postal ville
closing: Veuillez croire, Madame, Monsieur, à l’assurance de ma respectueuse considération.
lang: fr
papersize: a4
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi morbi tempus iaculis urna id volutpat lacus. Sit amet porttitor eget dolor morbi non. Nec sagittis aliquam malesuada bibendum. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Sed velit dignissim sodales ut eu sem integer. Tristique senectus et netus et. Fermentum leo vel orci porta non pulvinar. Viverra orci sagittis eu volutpat odio facilisis mauris sit amet. Quisque non tellus orci ac auctor augue mauris. Hendrerit gravida rutrum quisque non tellus orci ac auctor augue. Facilisis gravida neque convallis a cras. Imperdiet sed euismod nisi porta lorem mollis. Odio tempor orci dapibus ultrices. Blandit aliquam etiam erat velit scelerisque in dictum. At augue eget arcu dictum varius duis. Amet tellus cras adipiscing enim eu turpis egestas pretium aenean. Elementum curabitur vitae nunc sed velit.

Aliquam etiam erat velit scelerisque in dictum non consectetur a. Diam maecenas ultricies mi eget mauris pharetra et. Nulla facilisi nullam vehicula ipsum a. Sed augue lacus viverra vitae. At augue eget arcu dictum varius duis. Ut venenatis tellus in metus vulputate eu. At tempor commodo ullamcorper a lacus vestibulum sed arcu non. Platea dictumst quisque sagittis purus sit. Dis parturient montes nascetur ridiculus. Ut sem viverra aliquet eget. Varius vel pharetra vel turpis nunc eget. Et malesuada fames ac turpis.
