FROM jagregory/pandoc:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
apt-get install --yes --no-install-recommends texlive-lang-french
