DOCKER_IMAGE=pandoc-fr:latest
PANDOC=docker run --volume `pwd`:/source $(DOCKER_IMAGE)

all: lettre.pdf

%.pdf: %.md docker
	$(PANDOC) --template=templates/lettre.latex --latex-engine=xelatex $< --standalone --output $@

docker:
	docker build -t $(DOCKER_IMAGE) .


clean:
	rm -f *.pdf facturation2.md

