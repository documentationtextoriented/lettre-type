Permet de générer un document pdf , une lettre type à partir d'un fichier markdown.

Pré-requis:

* docker
* GNU Make

Pour l'utiliser :

```bash
make lettre.pdf
```
va créer le docker dérivé de jagregory/pandoc et convertir le markdown en pdf.
